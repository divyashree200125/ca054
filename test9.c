#include<stdio.h>
int main ()
{
    int x,y,*a,*b,temp;
    printf("enter the value of x and y \n");
    scanf("%d%d",&x,&y);
    printf("before swapping\n x=%d\n y=%d\n",x,y);
    a=&x;
    b=&y;
    temp=*a;
    *a=*b;
    *b=temp;
    printf("after swapping\n x=%d\n y=%d\n",x,y);
    return 0;
}