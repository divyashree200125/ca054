#include<stdio.h>
#include<string.h>
void isPalindrome(char str[]);
int main()
{
    char str[100];
    printf("\n Please enter any string:");
    gets(str);
    isPalindrome(str);
    return 0;
}
void isPalindrome(char str[])
{
    int i=0;
    int len=strlen(str)-1;
    while (len>i)
    {
        if(str[i++]!=str[len--])
        {
            printf("\n %s is not a Palindrome string",str);
            return;
        }
    }
    printf("\n %s is a Palindrome string",str);
}